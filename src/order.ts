import { AppDataSource } from "./data-source";
import { User } from "./entity/User";
import { Product } from "./entity/Product";
import { Order } from "./entity/Order";
import { OrderItem } from "./entity/OrderItem";

const orderDto = {
  OrderItems: [
    { productId: 1, qty: 1 },
    { productId: 2, qty: 2 },
    { productId: 3, qty: 1 },
  ],
  userId: 2,
};

AppDataSource.initialize()
  .then(async () => {
    const usersRepository = AppDataSource.getRepository(User);
    const productRepository = AppDataSource.getRepository(Product);
    const orderItemsRepository = AppDataSource.getRepository(OrderItem);
    const ordersRepository = AppDataSource.getRepository(Order);
    const user = await usersRepository.findOneBy({ id: orderDto.userId });

    const order = new Order();
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];
    for (const oi of orderDto.OrderItems) {
      const orderItem = new OrderItem();
      orderItem.product = await productRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.qty = oi.qty;
      orderItem.total = orderItem.price * orderItem.qty;
      await orderItemsRepository.save(orderItem);
      order.orderItems.push(orderItem);
      order.total += orderItem.total;
      order.qty += order.qty;
      await ordersRepository.save(order);
    }

    const orders = await ordersRepository.find({
      relations: { orderItems: true, user: true },
    });
    console.log(JSON.stringify(orders, null, 2));
  })
  .catch((error) => console.log(error));
